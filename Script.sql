/*==============================================================*/
/* DBMS name:      Microsoft SQL Server 2008                    */
/* Created on:     14.05.2017 19:43:07                          */
/*==============================================================*/


if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('AdminCredentials') and o.name = 'FK_ADMINCRE_ACCESSADM_LEVELACC')
alter table AdminCredentials
   drop constraint FK_ADMINCRE_ACCESSADM_LEVELACC
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Clients') and o.name = 'FK_CLIENTS_ACCESSCLI_LEVELACC')
alter table Clients
   drop constraint FK_CLIENTS_ACCESSCLI_LEVELACC
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('ClientsToSalon') and o.name = 'FK_CLIENTST_CLIENTSTO_CLIENTS')
alter table ClientsToSalon
   drop constraint FK_CLIENTST_CLIENTSTO_CLIENTS
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('ClientsToSalon') and o.name = 'FK_CLIENTST_CLIENTSTO_SALON')
alter table ClientsToSalon
   drop constraint FK_CLIENTST_CLIENTSTO_SALON
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Employers') and o.name = 'FK_EMPLOYER_ACCESSEMP_LEVELACC')
alter table Employers
   drop constraint FK_EMPLOYER_ACCESSEMP_LEVELACC
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Employers') and o.name = 'FK_EMPLOYER_MASTERWOR_SALON')
alter table Employers
   drop constraint FK_EMPLOYER_MASTERWOR_SALON
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Employers') and o.name = 'FK_EMPLOYER_TSIFTFORE_TYPESHIF')
alter table Employers
   drop constraint FK_EMPLOYER_TSIFTFORE_TYPESHIF
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Review') and o.name = 'FK_REVIEW_REVIEWCLI_CLIENTS')
alter table Review
   drop constraint FK_REVIEW_REVIEWCLI_CLIENTS
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Review') and o.name = 'FK_REVIEW_REVIEWOFS_SALON')
alter table Review
   drop constraint FK_REVIEW_REVIEWOFS_SALON
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Services') and o.name = 'FK_SERVICES_SERVICEST_SALON')
alter table Services
   drop constraint FK_SERVICES_SERVICEST_SALON
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Shift') and o.name = 'FK_SHIFT_GENERATES_TYPESHIF')
alter table Shift
   drop constraint FK_SHIFT_GENERATES_TYPESHIF
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Shift') and o.name = 'FK_SHIFT_SHIFTMAST_EMPLOYER')
alter table Shift
   drop constraint FK_SHIFT_SHIFTMAST_EMPLOYER
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Skills') and o.name = 'FK_SKILLS_MASTERSKI_EMPLOYER')
alter table Skills
   drop constraint FK_SKILLS_MASTERSKI_EMPLOYER
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Skills') and o.name = 'FK_SKILLS_SKILLSOFS_SERVICES')
alter table Skills
   drop constraint FK_SKILLS_SKILLSOFS_SERVICES
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('TypeShift') and o.name = 'FK_TYPESHIF_TSHIFTFOR_SALON')
alter table TypeShift
   drop constraint FK_TYPESHIF_TSHIFTFOR_SALON
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('VisitRecord') and o.name = 'FK_VISITREC_CLIENTVIS_CLIENTS')
alter table VisitRecord
   drop constraint FK_VISITREC_CLIENTVIS_CLIENTS
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('VisitRecord') and o.name = 'FK_VISITREC_VISITRONS_SKILLS')
alter table VisitRecord
   drop constraint FK_VISITREC_VISITRONS_SKILLS
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('VisitRecord') and o.name = 'FK_VISITREC_VISITRONS_SHIFT')
alter table VisitRecord
   drop constraint FK_VISITREC_VISITRONS_SHIFT
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('AdminCredentials')
            and   name  = 'AccessAdmins_FK'
            and   indid > 0
            and   indid < 255)
   drop index AdminCredentials.AccessAdmins_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('AdminCredentials')
            and   type = 'U')
   drop table AdminCredentials
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Clients')
            and   name  = 'AccessClients_FK'
            and   indid > 0
            and   indid < 255)
   drop index Clients.AccessClients_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Clients')
            and   type = 'U')
   drop table Clients
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('ClientsToSalon')
            and   name  = 'ClientsToSalon2_FK'
            and   indid > 0
            and   indid < 255)
   drop index ClientsToSalon.ClientsToSalon2_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('ClientsToSalon')
            and   type = 'U')
   drop table ClientsToSalon
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Employers')
            and   name  = 'AccessEmp_FK'
            and   indid > 0
            and   indid < 255)
   drop index Employers.AccessEmp_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Employers')
            and   name  = 'TSiftForEmp_FK'
            and   indid > 0
            and   indid < 255)
   drop index Employers.TSiftForEmp_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Employers')
            and   name  = 'MasterWorkToSalon_FK'
            and   indid > 0
            and   indid < 255)
   drop index Employers.MasterWorkToSalon_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Employers')
            and   type = 'U')
   drop table Employers
go

if exists (select 1
            from  sysobjects
           where  id = object_id('LevelAccess')
            and   type = 'U')
   drop table LevelAccess
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Review')
            and   name  = 'ReviewClients_FK'
            and   indid > 0
            and   indid < 255)
   drop index Review.ReviewClients_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Review')
            and   name  = 'ReviewOfSalon_FK'
            and   indid > 0
            and   indid < 255)
   drop index Review.ReviewOfSalon_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Review')
            and   type = 'U')
   drop table Review
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Salon')
            and   type = 'U')
   drop table Salon
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Services')
            and   name  = 'ServicesToSalon_FK'
            and   indid > 0
            and   indid < 255)
   drop index Services.ServicesToSalon_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Services')
            and   type = 'U')
   drop table Services
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Shift')
            and   name  = 'GenerateShift_FK'
            and   indid > 0
            and   indid < 255)
   drop index Shift.GenerateShift_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Shift')
            and   name  = 'ShiftMaster_FK'
            and   indid > 0
            and   indid < 255)
   drop index Shift.ShiftMaster_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Shift')
            and   type = 'U')
   drop table Shift
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Skills')
            and   name  = 'SkillsOfServices_FK'
            and   indid > 0
            and   indid < 255)
   drop index Skills.SkillsOfServices_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Skills')
            and   name  = 'MasterSkills_FK'
            and   indid > 0
            and   indid < 255)
   drop index Skills.MasterSkills_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Skills')
            and   type = 'U')
   drop table Skills
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('TypeShift')
            and   name  = 'TShiftForSalon_FK'
            and   indid > 0
            and   indid < 255)
   drop index TypeShift.TShiftForSalon_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('TypeShift')
            and   type = 'U')
   drop table TypeShift
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('VisitRecord')
            and   name  = 'VisitROnService_FK'
            and   indid > 0
            and   indid < 255)
   drop index VisitRecord.VisitROnService_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('VisitRecord')
            and   name  = 'VisitROnShift_FK'
            and   indid > 0
            and   indid < 255)
   drop index VisitRecord.VisitROnShift_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('VisitRecord')
            and   name  = 'ClientVisitR_FK'
            and   indid > 0
            and   indid < 255)
   drop index VisitRecord.ClientVisitR_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('VisitRecord')
            and   type = 'U')
   drop table VisitRecord
go

/*==============================================================*/
/* Table: AdminCredentials                                      */
/*==============================================================*/
create table AdminCredentials (
   ACred_ID             int                  identity,
   LA_ID                int                  not null,
   ACred_FullName       varchar(500)         not null,
   ACred_Phone          varchar(10)          not null,
   "ACred_E-mail"       varchar(500)         not null,
   ACred_MD5Password    varchar(32)          not null,
   ACred_Confirmed      bit                  not null,
   constraint PK_ADMINCREDENTIALS primary key nonclustered (ACred_ID)
)
go

/*==============================================================*/
/* Index: AccessAdmins_FK                                       */
/*==============================================================*/
create index AccessAdmins_FK on AdminCredentials (
LA_ID ASC
)
go

/*==============================================================*/
/* Table: Clients                                               */
/*==============================================================*/
create table Clients (
   Client_ID            int                  identity,
   LA_ID                int                  not null,
   Client_FullName      varchar(500)         not null,
   Client_Phone         varchar(10)          not null,
   "Client_E-mail"      varchar(500)         null,
   Client_MD5Password   varchar(32)          not null,
   Client_Confirmed     bit                  null,
   constraint PK_CLIENTS primary key nonclustered (Client_ID)
)
go

/*==============================================================*/
/* Index: AccessClients_FK                                      */
/*==============================================================*/
create index AccessClients_FK on Clients (
LA_ID ASC
)
go

/*==============================================================*/
/* Table: ClientsToSalon                                        */
/*==============================================================*/
create table ClientsToSalon (
   Client_ID            int                  not null,
   Salon_ID             int                  not null,
   constraint PK_CLIENTSTOSALON primary key nonclustered (Client_ID, Salon_ID)
)
go

/*==============================================================*/
/* Index: ClientsToSalon2_FK                                    */
/*==============================================================*/
create index ClientsToSalon2_FK on ClientsToSalon (
Salon_ID ASC
)
go

/*==============================================================*/
/* Table: Employers                                             */
/*==============================================================*/
create table Employers (
   Emp_ID               int                  identity,
   Salon_ID             int                  not null,
   LA_ID                int                  not null,
   TShift_ID            int                  null,
   Emp_FullName         varchar(500)         not null,
   Emp_Phone            varchar(10)          not null,
   Emp_Position         varchar(500)         not null,
   "Emp_E-mail"         varchar(500)         null,
   Emp_MD5Password      varchar(32)          not null,
   Emp_Confirmed        bit                  null,
   constraint PK_EMPLOYERS primary key nonclustered (Emp_ID)
)
go

/*==============================================================*/
/* Index: MasterWorkToSalon_FK                                  */
/*==============================================================*/
create index MasterWorkToSalon_FK on Employers (
Salon_ID ASC
)
go

/*==============================================================*/
/* Index: TSiftForEmp_FK                                        */
/*==============================================================*/
create index TSiftForEmp_FK on Employers (
TShift_ID ASC
)
go

/*==============================================================*/
/* Index: AccessEmp_FK                                          */
/*==============================================================*/
create index AccessEmp_FK on Employers (
LA_ID ASC
)
go

/*==============================================================*/
/* Table: LevelAccess                                           */
/*==============================================================*/
create table LevelAccess (
   LA_ID                int                  identity,
   LA_Title             varchar(500)         not null,
   LA_Capabilities      varchar(8000)        not null,
   constraint PK_LEVELACCESS primary key nonclustered (LA_ID)
)
go

/*==============================================================*/
/* Table: Review                                                */
/*==============================================================*/
create table Review (
   Rev_DT               datetime             not null,
   Salon_ID             int                  not null,
   Client_ID            int                  not null,
   Rev_Theme            varchar(500)         not null,
   Rev_Text             varchar(8000)        not null,
   constraint PK_REVIEW primary key nonclustered (Rev_DT)
)
go

/*==============================================================*/
/* Index: ReviewOfSalon_FK                                      */
/*==============================================================*/
create index ReviewOfSalon_FK on Review (
Salon_ID ASC
)
go

/*==============================================================*/
/* Index: ReviewClients_FK                                      */
/*==============================================================*/
create index ReviewClients_FK on Review (
Client_ID ASC
)
go

/*==============================================================*/
/* Table: Salon                                                 */
/*==============================================================*/
create table Salon (
   Salon_ID             int                  identity,
   Salon_Name           varchar(500)         not null,
   Salon_Address        varchar(1000)        not null,
   constraint PK_SALON primary key nonclustered (Salon_ID)
)
go

/*==============================================================*/
/* Table: Services                                              */
/*==============================================================*/
create table Services (
   Serv_ID              int                  identity,
   Salon_ID             int                  not null,
   Serv_Name            varchar(500)         not null,
   Serv_CostPrice       int                  not null,
   Serv_TimePlan        time                 not null,
   constraint PK_SERVICES primary key nonclustered (Serv_ID)
)
go

/*==============================================================*/
/* Index: ServicesToSalon_FK                                    */
/*==============================================================*/
create index ServicesToSalon_FK on Services (
Salon_ID ASC
)
go

/*==============================================================*/
/* Table: Shift                                                 */
/*==============================================================*/
create table Shift (
   Shift_ID             int                  identity,
   Emp_ID               int                  not null,
   TShift_ID            int                  not null,
   Shift_Date           date                 not null,
   Shift_BeginT         time                 not null,
   Shift_EndT           time                 not null,
   Shift_Status         bit                  not null,
   constraint PK_SHIFT primary key nonclustered (Shift_ID)
)
go

/*==============================================================*/
/* Index: ShiftMaster_FK                                        */
/*==============================================================*/
create index ShiftMaster_FK on Shift (
Emp_ID ASC
)
go

/*==============================================================*/
/* Index: GenerateShift_FK                                      */
/*==============================================================*/
create index GenerateShift_FK on Shift (
TShift_ID ASC
)
go

/*==============================================================*/
/* Table: Skills                                                */
/*==============================================================*/
create table Skills (
   Skills_ID            int                  identity,
   Emp_ID               int                  not null,
   Serv_ID              int                  not null,
   constraint PK_SKILLS primary key nonclustered (Skills_ID)
)
go

/*==============================================================*/
/* Index: MasterSkills_FK                                       */
/*==============================================================*/
create index MasterSkills_FK on Skills (
Emp_ID ASC
)
go

/*==============================================================*/
/* Index: SkillsOfServices_FK                                   */
/*==============================================================*/
create index SkillsOfServices_FK on Skills (
Serv_ID ASC
)
go

/*==============================================================*/
/* Table: TypeShift                                             */
/*==============================================================*/
create table TypeShift (
   TShift_ID            int                  not null,
   Salon_ID             int                  not null,
   TShift_Design        varchar(5)           not null,
   constraint PK_TYPESHIFT primary key nonclustered (TShift_ID)
)
go

/*==============================================================*/
/* Index: TShiftForSalon_FK                                     */
/*==============================================================*/
create index TShiftForSalon_FK on TypeShift (
Salon_ID ASC
)
go

/*==============================================================*/
/* Table: VisitRecord                                           */
/*==============================================================*/
create table VisitRecord (
   VisitR_ID            int                  identity,
   Client_ID            int                  not null,
   Shift_ID             int                  not null,
   Skills_ID            int                  not null,
   VisitR_BeginT        time                 not null,
   VisitR_EndT          time                 null,
   VisitR_CostFact      int                  null,
   VisitR_Status        bit                  not null,
   constraint PK_VISITRECORD primary key nonclustered (VisitR_ID)
)
go

/*==============================================================*/
/* Index: ClientVisitR_FK                                       */
/*==============================================================*/
create index ClientVisitR_FK on VisitRecord (
Client_ID ASC
)
go

/*==============================================================*/
/* Index: VisitROnShift_FK                                      */
/*==============================================================*/
create index VisitROnShift_FK on VisitRecord (
Shift_ID ASC
)
go

/*==============================================================*/
/* Index: VisitROnService_FK                                    */
/*==============================================================*/
create index VisitROnService_FK on VisitRecord (
Skills_ID ASC
)
go

alter table AdminCredentials
   add constraint FK_ADMINCRE_ACCESSADM_LEVELACC foreign key (LA_ID)
      references LevelAccess (LA_ID)
go

alter table Clients
   add constraint FK_CLIENTS_ACCESSCLI_LEVELACC foreign key (LA_ID)
      references LevelAccess (LA_ID)
go

alter table ClientsToSalon
   add constraint FK_CLIENTST_CLIENTSTO_CLIENTS foreign key (Client_ID)
      references Clients (Client_ID)
go

alter table ClientsToSalon
   add constraint FK_CLIENTST_CLIENTSTO_SALON foreign key (Salon_ID)
      references Salon (Salon_ID)
go

alter table Employers
   add constraint FK_EMPLOYER_ACCESSEMP_LEVELACC foreign key (LA_ID)
      references LevelAccess (LA_ID)
go

alter table Employers
   add constraint FK_EMPLOYER_MASTERWOR_SALON foreign key (Salon_ID)
      references Salon (Salon_ID)
go

alter table Employers
   add constraint FK_EMPLOYER_TSIFTFORE_TYPESHIF foreign key (TShift_ID)
      references TypeShift (TShift_ID)
go

alter table Review
   add constraint FK_REVIEW_REVIEWCLI_CLIENTS foreign key (Client_ID)
      references Clients (Client_ID)
go

alter table Review
   add constraint FK_REVIEW_REVIEWOFS_SALON foreign key (Salon_ID)
      references Salon (Salon_ID)
go

alter table Services
   add constraint FK_SERVICES_SERVICEST_SALON foreign key (Salon_ID)
      references Salon (Salon_ID)
go

alter table Shift
   add constraint FK_SHIFT_GENERATES_TYPESHIF foreign key (TShift_ID)
      references TypeShift (TShift_ID)
go

alter table Shift
   add constraint FK_SHIFT_SHIFTMAST_EMPLOYER foreign key (Emp_ID)
      references Employers (Emp_ID)
go

alter table Skills
   add constraint FK_SKILLS_MASTERSKI_EMPLOYER foreign key (Emp_ID)
      references Employers (Emp_ID)
go

alter table Skills
   add constraint FK_SKILLS_SKILLSOFS_SERVICES foreign key (Serv_ID)
      references Services (Serv_ID)
go

alter table TypeShift
   add constraint FK_TYPESHIF_TSHIFTFOR_SALON foreign key (Salon_ID)
      references Salon (Salon_ID)
go

alter table VisitRecord
   add constraint FK_VISITREC_CLIENTVIS_CLIENTS foreign key (Client_ID)
      references Clients (Client_ID)
go

alter table VisitRecord
   add constraint FK_VISITREC_VISITRONS_SKILLS foreign key (Skills_ID)
      references Skills (Skills_ID)
go

alter table VisitRecord
   add constraint FK_VISITREC_VISITRONS_SHIFT foreign key (Shift_ID)
      references Shift (Shift_ID)
go

