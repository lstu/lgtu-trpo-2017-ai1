
# Shift

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**shiftID** | **String** |  | 
**empID** | **String** |  | 
**shiftDate** | **String** |  | 
**shiftBeginT** | **String** |  | 
**shiftEndT** | **String** |  | 
**shiftStatus** | **String** |  | 



