# DirectorsApi

All URIs are relative to *https://virtserver.swaggerhub.com/NoVaS/Salun/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createserv**](DirectorsApi.md#createserv) | **POST** /DirectorsCreateServ | Create services
[**directorsDeleteEmpDelete**](DirectorsApi.md#directorsDeleteEmpDelete) | **DELETE** /DirectorsDeleteEmp | delite salon
[**directorsDeleteServDelete**](DirectorsApi.md#directorsDeleteServDelete) | **DELETE** /DirectorsDeleteServ | deleteserv
[**directorsEditEmpPut**](DirectorsApi.md#directorsEditEmpPut) | **PUT** /DirectorsEditEmp | update emp
[**directorsUpdateServPut**](DirectorsApi.md#directorsUpdateServPut) | **PUT** /DirectorsUpdateServ | edit serv
[**newemp**](DirectorsApi.md#newemp) | **POST** /DirectorsEmpReg | registr emp
[**posesh**](DirectorsApi.md#posesh) | **GET** /Directorsposeh | look psesh


<a name="createserv"></a>
# **createserv**
> createserv(creatserv)

Create services

create services

### Example
```java
// Import classes:
//import io.swagger.client.api.DirectorsApi;

DirectorsApi apiInstance = new DirectorsApi();
Services creatserv = new Services(); // Services | servises
try {
    apiInstance.createserv(creatserv);
} catch (ApiException e) {
    System.err.println("Exception when calling DirectorsApi#createserv");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **creatserv** | [**Services**](Services.md)| servises |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="directorsDeleteEmpDelete"></a>
# **directorsDeleteEmpDelete**
> directorsDeleteEmpDelete(empId)

delite salon

delete

### Example
```java
// Import classes:
//import io.swagger.client.api.DirectorsApi;

DirectorsApi apiInstance = new DirectorsApi();
String empId = "empId_example"; // String | id emp
try {
    apiInstance.directorsDeleteEmpDelete(empId);
} catch (ApiException e) {
    System.err.println("Exception when calling DirectorsApi#directorsDeleteEmpDelete");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **empId** | **String**| id emp |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="directorsDeleteServDelete"></a>
# **directorsDeleteServDelete**
> directorsDeleteServDelete(empId)

deleteserv

deleteserv

### Example
```java
// Import classes:
//import io.swagger.client.api.DirectorsApi;

DirectorsApi apiInstance = new DirectorsApi();
String empId = "empId_example"; // String | id emp
try {
    apiInstance.directorsDeleteServDelete(empId);
} catch (ApiException e) {
    System.err.println("Exception when calling DirectorsApi#directorsDeleteServDelete");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **empId** | **String**| id emp |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="directorsEditEmpPut"></a>
# **directorsEditEmpPut**
> directorsEditEmpPut(regsalon, levelacces, skills, shift)

update emp

update emp

### Example
```java
// Import classes:
//import io.swagger.client.api.DirectorsApi;

DirectorsApi apiInstance = new DirectorsApi();
Employers regsalon = new Employers(); // Employers | salon
String levelacces = "levelacces_example"; // String | levelacces
String skills = "skills_example"; // String | skilemp
String shift = "shift_example"; // String | shift
try {
    apiInstance.directorsEditEmpPut(regsalon, levelacces, skills, shift);
} catch (ApiException e) {
    System.err.println("Exception when calling DirectorsApi#directorsEditEmpPut");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **regsalon** | [**Employers**](Employers.md)| salon | [optional]
 **levelacces** | **String**| levelacces | [optional]
 **skills** | **String**| skilemp | [optional]
 **shift** | **String**| shift | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="directorsUpdateServPut"></a>
# **directorsUpdateServPut**
> directorsUpdateServPut(editserv)

edit serv

edit servises

### Example
```java
// Import classes:
//import io.swagger.client.api.DirectorsApi;

DirectorsApi apiInstance = new DirectorsApi();
Services editserv = new Services(); // Services | editserv
try {
    apiInstance.directorsUpdateServPut(editserv);
} catch (ApiException e) {
    System.err.println("Exception when calling DirectorsApi#directorsUpdateServPut");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **editserv** | [**Services**](Services.md)| editserv | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="newemp"></a>
# **newemp**
> newemp(regemp, levelacces, skills, shift)

registr emp

Registration new emp

### Example
```java
// Import classes:
//import io.swagger.client.api.DirectorsApi;

DirectorsApi apiInstance = new DirectorsApi();
Employers regemp = new Employers(); // Employers | emp
String levelacces = "levelacces_example"; // String | levelacces
String skills = "skills_example"; // String | skilemp
String shift = "shift_example"; // String | shift
try {
    apiInstance.newemp(regemp, levelacces, skills, shift);
} catch (ApiException e) {
    System.err.println("Exception when calling DirectorsApi#newemp");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **regemp** | [**Employers**](Employers.md)| emp |
 **levelacces** | **String**| levelacces |
 **skills** | **String**| skilemp |
 **shift** | **String**| shift |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="posesh"></a>
# **posesh**
> posesh(date)

look psesh

posesh

### Example
```java
// Import classes:
//import io.swagger.client.api.DirectorsApi;

DirectorsApi apiInstance = new DirectorsApi();
String date = "date_example"; // String | date
try {
    apiInstance.posesh(date);
} catch (ApiException e) {
    System.err.println("Exception when calling DirectorsApi#posesh");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **date** | **String**| date |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

