
# Review

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**revDT** | **String** |  | 
**salonID** | **String** |  | 
**clientID** | **String** |  | 
**revTheme** | **String** |  | 
**revText** | **String** |  | 



