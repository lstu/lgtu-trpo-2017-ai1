# ClientsApi

All URIs are relative to *https://virtserver.swaggerhub.com/NoVaS/Salun/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**clientdelitevisitDelete**](ClientsApi.md#clientdelitevisitDelete) | **DELETE** /Clientdelitevisit | delite visit
[**create**](ClientsApi.md#create) | **POST** /ClientsCreatezai | createzai
[**log**](ClientsApi.md#log) | **GET** /Login | login
[**look**](ClientsApi.md#look) | **GET** /ClientslookReview | look review
[**makerew**](ClientsApi.md#makerew) | **POST** /ClientsMakeReview | making rewiew
[**reg**](ClientsApi.md#reg) | **POST** /Registration | registering client


<a name="clientdelitevisitDelete"></a>
# **clientdelitevisitDelete**
> clientdelitevisitDelete(visitId)

delite visit

delete

### Example
```java
// Import classes:
//import io.swagger.client.api.ClientsApi;

ClientsApi apiInstance = new ClientsApi();
String visitId = "visitId_example"; // String | id visita
try {
    apiInstance.clientdelitevisitDelete(visitId);
} catch (ApiException e) {
    System.err.println("Exception when calling ClientsApi#clientdelitevisitDelete");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **visitId** | **String**| id visita |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="create"></a>
# **create**
> create(choosetime, chooseemp, chooseskl)

createzai

sozdaniezaiavki

### Example
```java
// Import classes:
//import io.swagger.client.api.ClientsApi;

ClientsApi apiInstance = new ClientsApi();
String choosetime = "choosetime_example"; // String | choosing time
String chooseemp = "chooseemp_example"; // String | Choosing employer
String chooseskl = "chooseskl_example"; // String | Choosing skills
try {
    apiInstance.create(choosetime, chooseemp, chooseskl);
} catch (ApiException e) {
    System.err.println("Exception when calling ClientsApi#create");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **choosetime** | **String**| choosing time |
 **chooseemp** | **String**| Choosing employer |
 **chooseskl** | **String**| Choosing skills |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="log"></a>
# **log**
> log(telephone, password)

login

vhod

### Example
```java
// Import classes:
//import io.swagger.client.api.ClientsApi;

ClientsApi apiInstance = new ClientsApi();
String telephone = "telephone_example"; // String | login
String password = "password_example"; // String | password
try {
    apiInstance.log(telephone, password);
} catch (ApiException e) {
    System.err.println("Exception when calling ClientsApi#log");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **telephone** | **String**| login |
 **password** | **String**| password |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="look"></a>
# **look**
> look(idReview)

look review

look at review

### Example
```java
// Import classes:
//import io.swagger.client.api.ClientsApi;

ClientsApi apiInstance = new ClientsApi();
String idReview = "idReview_example"; // String | id review
try {
    apiInstance.look(idReview);
} catch (ApiException e) {
    System.err.println("Exception when calling ClientsApi#look");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idReview** | **String**| id review |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="makerew"></a>
# **makerew**
> makerew(time, rewiew)

making rewiew

making rewiew

### Example
```java
// Import classes:
//import io.swagger.client.api.ClientsApi;

ClientsApi apiInstance = new ClientsApi();
String time = "time_example"; // String | timevisita
String rewiew = "rewiew_example"; // String | examp
try {
    apiInstance.makerew(time, rewiew);
} catch (ApiException e) {
    System.err.println("Exception when calling ClientsApi#makerew");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **time** | **String**| timevisita |
 **rewiew** | **String**| examp |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="reg"></a>
# **reg**
> reg(registrclient)

registering client

registering client

### Example
```java
// Import classes:
//import io.swagger.client.api.ClientsApi;

ClientsApi apiInstance = new ClientsApi();
Clients registrclient = new Clients(); // Clients | registr client
try {
    apiInstance.reg(registrclient);
} catch (ApiException e) {
    System.err.println("Exception when calling ClientsApi#reg");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **registrclient** | [**Clients**](Clients.md)| registr client |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

