
# AdminCredentials

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**aCredID** | **String** |  | 
**LA_ID** | **String** |  | 
**aCredFullname** | **String** |  | 
**aCredEMail** | **String** |  | 
**aCredPhone** | **String** |  | 
**aCredMD5Password** | **String** |  | 
**aCredConfirmed** | **String** |  | 



