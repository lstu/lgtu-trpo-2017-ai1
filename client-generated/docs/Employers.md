
# Employers

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**empID** | **String** |  | 
**salonID** | **String** |  | 
**LA_ID** | **String** |  | 
**empFullname** | **String** |  | 
**empEMail** | **String** |  | 
**empPosition** | **String** |  | 
**empPhone** | **String** |  | 
**empMD5Password** | **String** |  | 
**empConfirmed** | **String** |  | 



