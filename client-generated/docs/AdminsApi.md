# AdminsApi

All URIs are relative to *https://virtserver.swaggerhub.com/NoVaS/Salun/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**clientvisitstoryGet**](AdminsApi.md#clientvisitstoryGet) | **GET** /clientvisitstory | story
[**create**](AdminsApi.md#create) | **POST** /ClientsCreatezai | createzai
[**findclientGet**](AdminsApi.md#findclientGet) | **GET** /Findclient | find
[**markemp**](AdminsApi.md#markemp) | **PUT** /AdminsMarkEmp | marking employers
[**reg**](AdminsApi.md#reg) | **POST** /Registration | registering client


<a name="clientvisitstoryGet"></a>
# **clientvisitstoryGet**
> clientvisitstoryGet(story)

story

story of visit

### Example
```java
// Import classes:
//import io.swagger.client.api.AdminsApi;

AdminsApi apiInstance = new AdminsApi();
VisitRecord story = new VisitRecord(); // VisitRecord | story client
try {
    apiInstance.clientvisitstoryGet(story);
} catch (ApiException e) {
    System.err.println("Exception when calling AdminsApi#clientvisitstoryGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **story** | [**VisitRecord**](VisitRecord.md)| story client |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="create"></a>
# **create**
> create(choosetime, chooseemp, chooseskl)

createzai

sozdaniezaiavki

### Example
```java
// Import classes:
//import io.swagger.client.api.AdminsApi;

AdminsApi apiInstance = new AdminsApi();
String choosetime = "choosetime_example"; // String | choosing time
String chooseemp = "chooseemp_example"; // String | Choosing employer
String chooseskl = "chooseskl_example"; // String | Choosing skills
try {
    apiInstance.create(choosetime, chooseemp, chooseskl);
} catch (ApiException e) {
    System.err.println("Exception when calling AdminsApi#create");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **choosetime** | **String**| choosing time |
 **chooseemp** | **String**| Choosing employer |
 **chooseskl** | **String**| Choosing skills |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="findclientGet"></a>
# **findclientGet**
> findclientGet(clientName, clientSecondname, clientLastname)

find

findclients

### Example
```java
// Import classes:
//import io.swagger.client.api.AdminsApi;

AdminsApi apiInstance = new AdminsApi();
String clientName = "clientName_example"; // String | name clienta
String clientSecondname = "clientSecondname_example"; // String | secondname clienta
String clientLastname = "clientLastname_example"; // String | lastname clienta
try {
    apiInstance.findclientGet(clientName, clientSecondname, clientLastname);
} catch (ApiException e) {
    System.err.println("Exception when calling AdminsApi#findclientGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientName** | **String**| name clienta |
 **clientSecondname** | **String**| secondname clienta |
 **clientLastname** | **String**| lastname clienta |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="markemp"></a>
# **markemp**
> markemp(markemp)

marking employers

marking employers

### Example
```java
// Import classes:
//import io.swagger.client.api.AdminsApi;

AdminsApi apiInstance = new AdminsApi();
String markemp = "markemp_example"; // String | marking employers
try {
    apiInstance.markemp(markemp);
} catch (ApiException e) {
    System.err.println("Exception when calling AdminsApi#markemp");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **markemp** | **String**| marking employers |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="reg"></a>
# **reg**
> reg(registrclient)

registering client

registering client

### Example
```java
// Import classes:
//import io.swagger.client.api.AdminsApi;

AdminsApi apiInstance = new AdminsApi();
Clients registrclient = new Clients(); // Clients | registr client
try {
    apiInstance.reg(registrclient);
} catch (ApiException e) {
    System.err.println("Exception when calling AdminsApi#reg");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **registrclient** | [**Clients**](Clients.md)| registr client |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

