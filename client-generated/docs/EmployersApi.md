# EmployersApi

All URIs are relative to *https://virtserver.swaggerhub.com/NoVaS/Salun/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**log**](EmployersApi.md#log) | **GET** /Login | login
[**view**](EmployersApi.md#view) | **GET** /EmployersView | view


<a name="log"></a>
# **log**
> log(telephone, password)

login

vhod

### Example
```java
// Import classes:
//import io.swagger.client.api.EmployersApi;

EmployersApi apiInstance = new EmployersApi();
String telephone = "telephone_example"; // String | login
String password = "password_example"; // String | password
try {
    apiInstance.log(telephone, password);
} catch (ApiException e) {
    System.err.println("Exception when calling EmployersApi#log");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **telephone** | **String**| login |
 **password** | **String**| password |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="view"></a>
# **view**
> view(viewShift, viewvisit)

view

view

### Example
```java
// Import classes:
//import io.swagger.client.api.EmployersApi;

EmployersApi apiInstance = new EmployersApi();
String viewShift = "viewShift_example"; // String | view shift
String viewvisit = "viewvisit_example"; // String | view visit record
try {
    apiInstance.view(viewShift, viewvisit);
} catch (ApiException e) {
    System.err.println("Exception when calling EmployersApi#view");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **viewShift** | **String**| view shift |
 **viewvisit** | **String**| view visit record |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

