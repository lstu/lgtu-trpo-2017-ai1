
# VisitRecord

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**visitRID** | **String** |  | 
**clientID** | **String** |  | 
**shiftID** | **String** |  | 
**skillsID** | **String** |  | 
**visitRBeginT** | **String** |  | 
**visitREndT** | **String** |  | 
**visitRCostFact** | **String** |  | 
**visitRStatus** | **String** |  | 



