# AdministrationApi

All URIs are relative to *https://virtserver.swaggerhub.com/NoVaS/Salun/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteSalonDelete**](AdministrationApi.md#deleteSalonDelete) | **DELETE** /DeleteSalon | delite salon
[**newsalon**](AdministrationApi.md#newsalon) | **POST** /SalonReg | registr salon
[**supprew**](AdministrationApi.md#supprew) | **GET** /AdminSupport | Supporting in reviews
[**updatesalonPut**](AdministrationApi.md#updatesalonPut) | **PUT** /Updatesalon | update salon


<a name="deleteSalonDelete"></a>
# **deleteSalonDelete**
> deleteSalonDelete(salonId)

delite salon

delete

### Example
```java
// Import classes:
//import io.swagger.client.api.AdministrationApi;

AdministrationApi apiInstance = new AdministrationApi();
String salonId = "salonId_example"; // String | id salona
try {
    apiInstance.deleteSalonDelete(salonId);
} catch (ApiException e) {
    System.err.println("Exception when calling AdministrationApi#deleteSalonDelete");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **salonId** | **String**| id salona |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="newsalon"></a>
# **newsalon**
> newsalon(telephone, password, dirctorsName, directorsSecondName, directorsLastName, regsalon)

registr salon

Registration new salon

### Example
```java
// Import classes:
//import io.swagger.client.api.AdministrationApi;

AdministrationApi apiInstance = new AdministrationApi();
String telephone = "telephone_example"; // String | tel
String password = "password_example"; // String | password
String dirctorsName = "dirctorsName_example"; // String | directors name
String directorsSecondName = "directorsSecondName_example"; // String | second name
String directorsLastName = "directorsLastName_example"; // String | last name
Salon regsalon = new Salon(); // Salon | salon
try {
    apiInstance.newsalon(telephone, password, dirctorsName, directorsSecondName, directorsLastName, regsalon);
} catch (ApiException e) {
    System.err.println("Exception when calling AdministrationApi#newsalon");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **telephone** | **String**| tel |
 **password** | **String**| password |
 **dirctorsName** | **String**| directors name |
 **directorsSecondName** | **String**| second name |
 **directorsLastName** | **String**| last name |
 **regsalon** | [**Salon**](Salon.md)| salon |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="supprew"></a>
# **supprew**
> List&lt;Review&gt; supprew(supprew)

Supporting in reviews

Supporting in reviews

### Example
```java
// Import classes:
//import io.swagger.client.api.AdministrationApi;

AdministrationApi apiInstance = new AdministrationApi();
String supprew = "supprew_example"; // String | Supporting in reviews
try {
    List<Review> result = apiInstance.supprew(supprew);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AdministrationApi#supprew");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **supprew** | **String**| Supporting in reviews |

### Return type

[**List&lt;Review&gt;**](Review.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="updatesalonPut"></a>
# **updatesalonPut**
> updatesalonPut(salonId, regsalon)

update salon

update salon

### Example
```java
// Import classes:
//import io.swagger.client.api.AdministrationApi;

AdministrationApi apiInstance = new AdministrationApi();
String salonId = "salonId_example"; // String | id salona
Salon regsalon = new Salon(); // Salon | salon
try {
    apiInstance.updatesalonPut(salonId, regsalon);
} catch (ApiException e) {
    System.err.println("Exception when calling AdministrationApi#updatesalonPut");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **salonId** | **String**| id salona | [optional]
 **regsalon** | [**Salon**](Salon.md)| salon | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

