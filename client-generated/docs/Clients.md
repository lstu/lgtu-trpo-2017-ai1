
# Clients

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**clientID** | **String** |  | 
**LA_ID** | **String** |  | 
**clientFullname** | **String** |  | 
**clientEMail** | **String** |  | 
**clientPhone** | **String** |  | 
**clientMD5Password** | **String** |  | 
**clientConfirmed** | **String** |  | 



