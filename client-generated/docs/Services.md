
# Services

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**servID** | **String** |  | 
**salonID** | **String** |  | 
**servName** | **String** |  | 
**servCostprice** | **String** |  | 
**servTimeplan** | **String** |  | 



