/**
 * Simple Inventory API
 * This is a simple API
 *
 * OpenAPI spec version: 1.0.0
 * Contact: you@your-company.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.swagger.client.model;

import io.swagger.annotations.*;
import com.google.gson.annotations.SerializedName;

@ApiModel(description = "")
public class Review {
  
  @SerializedName("Rev_DT")
  private String revDT = null;
  @SerializedName("Salon_ID")
  private String salonID = null;
  @SerializedName("Client_ID")
  private String clientID = null;
  @SerializedName("Rev_Theme")
  private String revTheme = null;
  @SerializedName("Rev_Text")
  private String revText = null;

  /**
   **/
  @ApiModelProperty(required = true, value = "")
  public String getRevDT() {
    return revDT;
  }
  public void setRevDT(String revDT) {
    this.revDT = revDT;
  }

  /**
   **/
  @ApiModelProperty(required = true, value = "")
  public String getSalonID() {
    return salonID;
  }
  public void setSalonID(String salonID) {
    this.salonID = salonID;
  }

  /**
   **/
  @ApiModelProperty(required = true, value = "")
  public String getClientID() {
    return clientID;
  }
  public void setClientID(String clientID) {
    this.clientID = clientID;
  }

  /**
   **/
  @ApiModelProperty(required = true, value = "")
  public String getRevTheme() {
    return revTheme;
  }
  public void setRevTheme(String revTheme) {
    this.revTheme = revTheme;
  }

  /**
   **/
  @ApiModelProperty(required = true, value = "")
  public String getRevText() {
    return revText;
  }
  public void setRevText(String revText) {
    this.revText = revText;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Review review = (Review) o;
    return (this.revDT == null ? review.revDT == null : this.revDT.equals(review.revDT)) &&
        (this.salonID == null ? review.salonID == null : this.salonID.equals(review.salonID)) &&
        (this.clientID == null ? review.clientID == null : this.clientID.equals(review.clientID)) &&
        (this.revTheme == null ? review.revTheme == null : this.revTheme.equals(review.revTheme)) &&
        (this.revText == null ? review.revText == null : this.revText.equals(review.revText));
  }

  @Override
  public int hashCode() {
    int result = 17;
    result = 31 * result + (this.revDT == null ? 0: this.revDT.hashCode());
    result = 31 * result + (this.salonID == null ? 0: this.salonID.hashCode());
    result = 31 * result + (this.clientID == null ? 0: this.clientID.hashCode());
    result = 31 * result + (this.revTheme == null ? 0: this.revTheme.hashCode());
    result = 31 * result + (this.revText == null ? 0: this.revText.hashCode());
    return result;
  }

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class Review {\n");
    
    sb.append("  revDT: ").append(revDT).append("\n");
    sb.append("  salonID: ").append(salonID).append("\n");
    sb.append("  clientID: ").append(clientID).append("\n");
    sb.append("  revTheme: ").append(revTheme).append("\n");
    sb.append("  revText: ").append(revText).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}
