/**
 * Simple Inventory API
 * This is a simple API
 *
 * OpenAPI spec version: 1.0.0
 * Contact: you@your-company.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.swagger.client.model;

import io.swagger.annotations.*;
import com.google.gson.annotations.SerializedName;

@ApiModel(description = "")
public class AdminCredentials {
  
  @SerializedName("ACred_ID")
  private String aCredID = null;
  @SerializedName("LA_ID")
  private String LA_ID = null;
  @SerializedName("ACred_Fullname")
  private String aCredFullname = null;
  @SerializedName("ACred_E-mail")
  private String aCredEMail = null;
  @SerializedName("ACred_Phone")
  private String aCredPhone = null;
  @SerializedName("ACred_MD5Password")
  private String aCredMD5Password = null;
  @SerializedName("ACred_Confirmed")
  private String aCredConfirmed = null;

  /**
   **/
  @ApiModelProperty(required = true, value = "")
  public String getACredID() {
    return aCredID;
  }
  public void setACredID(String aCredID) {
    this.aCredID = aCredID;
  }

  /**
   **/
  @ApiModelProperty(required = true, value = "")
  public String getLAID() {
    return LA_ID;
  }
  public void setLAID(String LA_ID) {
    this.LA_ID = LA_ID;
  }

  /**
   **/
  @ApiModelProperty(required = true, value = "")
  public String getACredFullname() {
    return aCredFullname;
  }
  public void setACredFullname(String aCredFullname) {
    this.aCredFullname = aCredFullname;
  }

  /**
   **/
  @ApiModelProperty(required = true, value = "")
  public String getACredEMail() {
    return aCredEMail;
  }
  public void setACredEMail(String aCredEMail) {
    this.aCredEMail = aCredEMail;
  }

  /**
   **/
  @ApiModelProperty(required = true, value = "")
  public String getACredPhone() {
    return aCredPhone;
  }
  public void setACredPhone(String aCredPhone) {
    this.aCredPhone = aCredPhone;
  }

  /**
   **/
  @ApiModelProperty(required = true, value = "")
  public String getACredMD5Password() {
    return aCredMD5Password;
  }
  public void setACredMD5Password(String aCredMD5Password) {
    this.aCredMD5Password = aCredMD5Password;
  }

  /**
   **/
  @ApiModelProperty(required = true, value = "")
  public String getACredConfirmed() {
    return aCredConfirmed;
  }
  public void setACredConfirmed(String aCredConfirmed) {
    this.aCredConfirmed = aCredConfirmed;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AdminCredentials adminCredentials = (AdminCredentials) o;
    return (this.aCredID == null ? adminCredentials.aCredID == null : this.aCredID.equals(adminCredentials.aCredID)) &&
        (this.LA_ID == null ? adminCredentials.LA_ID == null : this.LA_ID.equals(adminCredentials.LA_ID)) &&
        (this.aCredFullname == null ? adminCredentials.aCredFullname == null : this.aCredFullname.equals(adminCredentials.aCredFullname)) &&
        (this.aCredEMail == null ? adminCredentials.aCredEMail == null : this.aCredEMail.equals(adminCredentials.aCredEMail)) &&
        (this.aCredPhone == null ? adminCredentials.aCredPhone == null : this.aCredPhone.equals(adminCredentials.aCredPhone)) &&
        (this.aCredMD5Password == null ? adminCredentials.aCredMD5Password == null : this.aCredMD5Password.equals(adminCredentials.aCredMD5Password)) &&
        (this.aCredConfirmed == null ? adminCredentials.aCredConfirmed == null : this.aCredConfirmed.equals(adminCredentials.aCredConfirmed));
  }

  @Override
  public int hashCode() {
    int result = 17;
    result = 31 * result + (this.aCredID == null ? 0: this.aCredID.hashCode());
    result = 31 * result + (this.LA_ID == null ? 0: this.LA_ID.hashCode());
    result = 31 * result + (this.aCredFullname == null ? 0: this.aCredFullname.hashCode());
    result = 31 * result + (this.aCredEMail == null ? 0: this.aCredEMail.hashCode());
    result = 31 * result + (this.aCredPhone == null ? 0: this.aCredPhone.hashCode());
    result = 31 * result + (this.aCredMD5Password == null ? 0: this.aCredMD5Password.hashCode());
    result = 31 * result + (this.aCredConfirmed == null ? 0: this.aCredConfirmed.hashCode());
    return result;
  }

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class AdminCredentials {\n");
    
    sb.append("  aCredID: ").append(aCredID).append("\n");
    sb.append("  LA_ID: ").append(LA_ID).append("\n");
    sb.append("  aCredFullname: ").append(aCredFullname).append("\n");
    sb.append("  aCredEMail: ").append(aCredEMail).append("\n");
    sb.append("  aCredPhone: ").append(aCredPhone).append("\n");
    sb.append("  aCredMD5Password: ").append(aCredMD5Password).append("\n");
    sb.append("  aCredConfirmed: ").append(aCredConfirmed).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}
