/**
 * Simple Inventory API
 * This is a simple API
 *
 * OpenAPI spec version: 1.0.0
 * Contact: you@your-company.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.swagger.client.api;

import io.swagger.client.ApiInvoker;
import io.swagger.client.ApiException;
import io.swagger.client.Pair;

import io.swagger.client.model.*;

import java.util.*;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import io.swagger.client.model.Review;
import io.swagger.client.model.Salon;

import org.apache.http.HttpEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

public class AdministrationApi {
  String basePath = "https://virtserver.swaggerhub.com/NoVaS/Salun/1.0.0";
  ApiInvoker apiInvoker = ApiInvoker.getInstance();

  public void addHeader(String key, String value) {
    getInvoker().addDefaultHeader(key, value);
  }

  public ApiInvoker getInvoker() {
    return apiInvoker;
  }

  public void setBasePath(String basePath) {
    this.basePath = basePath;
  }

  public String getBasePath() {
    return basePath;
  }

  /**
  * delite salon
  * delete
   * @param salonId id salona
   * @return void
  */
  public void deleteSalonDelete (String salonId) throws TimeoutException, ExecutionException, InterruptedException, ApiException {
     Object postBody = null;
  
      // verify the required parameter 'salonId' is set
      if (salonId == null) {
      VolleyError error = new VolleyError("Missing the required parameter 'salonId' when calling deleteSalonDelete",
      new ApiException(400, "Missing the required parameter 'salonId' when calling deleteSalonDelete"));
      }
  

  // create path and map variables
  String path = "/DeleteSalon".replaceAll("\\{format\\}","json");

  // query params
  List<Pair> queryParams = new ArrayList<Pair>();
      // header params
      Map<String, String> headerParams = new HashMap<String, String>();
      // form params
      Map<String, String> formParams = new HashMap<String, String>();

          queryParams.addAll(ApiInvoker.parameterToPairs("", "salon id", salonId));


      String[] contentTypes = {
  
      };
      String contentType = contentTypes.length > 0 ? contentTypes[0] : "application/json";

      if (contentType.startsWith("multipart/form-data")) {
      // file uploading
      MultipartEntityBuilder localVarBuilder = MultipartEntityBuilder.create();
  

      HttpEntity httpEntity = localVarBuilder.build();
      postBody = httpEntity;
      } else {
      // normal form params
        }

      String[] authNames = new String[] {  };

      try {
        String localVarResponse = apiInvoker.invokeAPI (basePath, path, "DELETE", queryParams, postBody, headerParams, formParams, contentType, authNames);
        if(localVarResponse != null){
           return ;
        } else {
           return ;
        }
      } catch (ApiException ex) {
         throw ex;
      } catch (InterruptedException ex) {
         throw ex;
      } catch (ExecutionException ex) {
         if(ex.getCause() instanceof VolleyError) {
	    VolleyError volleyError = (VolleyError)ex.getCause();
	    if (volleyError.networkResponse != null) {
	       throw new ApiException(volleyError.networkResponse.statusCode, volleyError.getMessage());
	    }
         }
         throw ex;
      } catch (TimeoutException ex) {
         throw ex;
      }
  }

      /**
   * delite salon
   * delete
   * @param salonId id salona
  */
  public void deleteSalonDelete (String salonId, final Response.Listener<String> responseListener, final Response.ErrorListener errorListener) {
    Object postBody = null;

  
    // verify the required parameter 'salonId' is set
    if (salonId == null) {
       VolleyError error = new VolleyError("Missing the required parameter 'salonId' when calling deleteSalonDelete",
         new ApiException(400, "Missing the required parameter 'salonId' when calling deleteSalonDelete"));
    }
    

    // create path and map variables
    String path = "/DeleteSalon".replaceAll("\\{format\\}","json");

    // query params
    List<Pair> queryParams = new ArrayList<Pair>();
    // header params
    Map<String, String> headerParams = new HashMap<String, String>();
    // form params
    Map<String, String> formParams = new HashMap<String, String>();

    queryParams.addAll(ApiInvoker.parameterToPairs("", "salon id", salonId));


    String[] contentTypes = {
      
    };
    String contentType = contentTypes.length > 0 ? contentTypes[0] : "application/json";

    if (contentType.startsWith("multipart/form-data")) {
      // file uploading
      MultipartEntityBuilder localVarBuilder = MultipartEntityBuilder.create();
      

      HttpEntity httpEntity = localVarBuilder.build();
      postBody = httpEntity;
    } else {
      // normal form params
          }

      String[] authNames = new String[] {  };

    try {
      apiInvoker.invokeAPI(basePath, path, "DELETE", queryParams, postBody, headerParams, formParams, contentType, authNames,
        new Response.Listener<String>() {
          @Override
          public void onResponse(String localVarResponse) {
              responseListener.onResponse(localVarResponse);
          }
      }, new Response.ErrorListener() {
          @Override
          public void onErrorResponse(VolleyError error) {
            errorListener.onErrorResponse(error);
          }
      });
    } catch (ApiException ex) {
      errorListener.onErrorResponse(new VolleyError(ex));
    }
  }
  /**
  * registr salon
  * Registration new salon
   * @param telephone tel
   * @param password password
   * @param dirctorsName directors name
   * @param directorsSecondName second name
   * @param directorsLastName last name
   * @param regsalon salon
   * @return void
  */
  public void newsalon (String telephone, String password, String dirctorsName, String directorsSecondName, String directorsLastName, Salon regsalon) throws TimeoutException, ExecutionException, InterruptedException, ApiException {
     Object postBody = regsalon;
  
      // verify the required parameter 'telephone' is set
      if (telephone == null) {
      VolleyError error = new VolleyError("Missing the required parameter 'telephone' when calling newsalon",
      new ApiException(400, "Missing the required parameter 'telephone' when calling newsalon"));
      }
  
      // verify the required parameter 'password' is set
      if (password == null) {
      VolleyError error = new VolleyError("Missing the required parameter 'password' when calling newsalon",
      new ApiException(400, "Missing the required parameter 'password' when calling newsalon"));
      }
  
      // verify the required parameter 'dirctorsName' is set
      if (dirctorsName == null) {
      VolleyError error = new VolleyError("Missing the required parameter 'dirctorsName' when calling newsalon",
      new ApiException(400, "Missing the required parameter 'dirctorsName' when calling newsalon"));
      }
  
      // verify the required parameter 'directorsSecondName' is set
      if (directorsSecondName == null) {
      VolleyError error = new VolleyError("Missing the required parameter 'directorsSecondName' when calling newsalon",
      new ApiException(400, "Missing the required parameter 'directorsSecondName' when calling newsalon"));
      }
  
      // verify the required parameter 'directorsLastName' is set
      if (directorsLastName == null) {
      VolleyError error = new VolleyError("Missing the required parameter 'directorsLastName' when calling newsalon",
      new ApiException(400, "Missing the required parameter 'directorsLastName' when calling newsalon"));
      }
  
      // verify the required parameter 'regsalon' is set
      if (regsalon == null) {
      VolleyError error = new VolleyError("Missing the required parameter 'regsalon' when calling newsalon",
      new ApiException(400, "Missing the required parameter 'regsalon' when calling newsalon"));
      }
  

  // create path and map variables
  String path = "/SalonReg".replaceAll("\\{format\\}","json");

  // query params
  List<Pair> queryParams = new ArrayList<Pair>();
      // header params
      Map<String, String> headerParams = new HashMap<String, String>();
      // form params
      Map<String, String> formParams = new HashMap<String, String>();

          queryParams.addAll(ApiInvoker.parameterToPairs("", "telephone", telephone));
          queryParams.addAll(ApiInvoker.parameterToPairs("", "password", password));
          queryParams.addAll(ApiInvoker.parameterToPairs("", "dirctors name", dirctorsName));
          queryParams.addAll(ApiInvoker.parameterToPairs("", "directors second name", directorsSecondName));
          queryParams.addAll(ApiInvoker.parameterToPairs("", "directors last name", directorsLastName));


      String[] contentTypes = {
  
      };
      String contentType = contentTypes.length > 0 ? contentTypes[0] : "application/json";

      if (contentType.startsWith("multipart/form-data")) {
      // file uploading
      MultipartEntityBuilder localVarBuilder = MultipartEntityBuilder.create();
  

      HttpEntity httpEntity = localVarBuilder.build();
      postBody = httpEntity;
      } else {
      // normal form params
        }

      String[] authNames = new String[] {  };

      try {
        String localVarResponse = apiInvoker.invokeAPI (basePath, path, "POST", queryParams, postBody, headerParams, formParams, contentType, authNames);
        if(localVarResponse != null){
           return ;
        } else {
           return ;
        }
      } catch (ApiException ex) {
         throw ex;
      } catch (InterruptedException ex) {
         throw ex;
      } catch (ExecutionException ex) {
         if(ex.getCause() instanceof VolleyError) {
	    VolleyError volleyError = (VolleyError)ex.getCause();
	    if (volleyError.networkResponse != null) {
	       throw new ApiException(volleyError.networkResponse.statusCode, volleyError.getMessage());
	    }
         }
         throw ex;
      } catch (TimeoutException ex) {
         throw ex;
      }
  }

      /**
   * registr salon
   * Registration new salon
   * @param telephone tel   * @param password password   * @param dirctorsName directors name   * @param directorsSecondName second name   * @param directorsLastName last name   * @param regsalon salon
  */
  public void newsalon (String telephone, String password, String dirctorsName, String directorsSecondName, String directorsLastName, Salon regsalon, final Response.Listener<String> responseListener, final Response.ErrorListener errorListener) {
    Object postBody = regsalon;

  
    // verify the required parameter 'telephone' is set
    if (telephone == null) {
       VolleyError error = new VolleyError("Missing the required parameter 'telephone' when calling newsalon",
         new ApiException(400, "Missing the required parameter 'telephone' when calling newsalon"));
    }
    
    // verify the required parameter 'password' is set
    if (password == null) {
       VolleyError error = new VolleyError("Missing the required parameter 'password' when calling newsalon",
         new ApiException(400, "Missing the required parameter 'password' when calling newsalon"));
    }
    
    // verify the required parameter 'dirctorsName' is set
    if (dirctorsName == null) {
       VolleyError error = new VolleyError("Missing the required parameter 'dirctorsName' when calling newsalon",
         new ApiException(400, "Missing the required parameter 'dirctorsName' when calling newsalon"));
    }
    
    // verify the required parameter 'directorsSecondName' is set
    if (directorsSecondName == null) {
       VolleyError error = new VolleyError("Missing the required parameter 'directorsSecondName' when calling newsalon",
         new ApiException(400, "Missing the required parameter 'directorsSecondName' when calling newsalon"));
    }
    
    // verify the required parameter 'directorsLastName' is set
    if (directorsLastName == null) {
       VolleyError error = new VolleyError("Missing the required parameter 'directorsLastName' when calling newsalon",
         new ApiException(400, "Missing the required parameter 'directorsLastName' when calling newsalon"));
    }
    
    // verify the required parameter 'regsalon' is set
    if (regsalon == null) {
       VolleyError error = new VolleyError("Missing the required parameter 'regsalon' when calling newsalon",
         new ApiException(400, "Missing the required parameter 'regsalon' when calling newsalon"));
    }
    

    // create path and map variables
    String path = "/SalonReg".replaceAll("\\{format\\}","json");

    // query params
    List<Pair> queryParams = new ArrayList<Pair>();
    // header params
    Map<String, String> headerParams = new HashMap<String, String>();
    // form params
    Map<String, String> formParams = new HashMap<String, String>();

    queryParams.addAll(ApiInvoker.parameterToPairs("", "telephone", telephone));
    queryParams.addAll(ApiInvoker.parameterToPairs("", "password", password));
    queryParams.addAll(ApiInvoker.parameterToPairs("", "dirctors name", dirctorsName));
    queryParams.addAll(ApiInvoker.parameterToPairs("", "directors second name", directorsSecondName));
    queryParams.addAll(ApiInvoker.parameterToPairs("", "directors last name", directorsLastName));


    String[] contentTypes = {
      
    };
    String contentType = contentTypes.length > 0 ? contentTypes[0] : "application/json";

    if (contentType.startsWith("multipart/form-data")) {
      // file uploading
      MultipartEntityBuilder localVarBuilder = MultipartEntityBuilder.create();
      

      HttpEntity httpEntity = localVarBuilder.build();
      postBody = httpEntity;
    } else {
      // normal form params
          }

      String[] authNames = new String[] {  };

    try {
      apiInvoker.invokeAPI(basePath, path, "POST", queryParams, postBody, headerParams, formParams, contentType, authNames,
        new Response.Listener<String>() {
          @Override
          public void onResponse(String localVarResponse) {
              responseListener.onResponse(localVarResponse);
          }
      }, new Response.ErrorListener() {
          @Override
          public void onErrorResponse(VolleyError error) {
            errorListener.onErrorResponse(error);
          }
      });
    } catch (ApiException ex) {
      errorListener.onErrorResponse(new VolleyError(ex));
    }
  }
  /**
  * Supporting in reviews
  * Supporting in reviews
   * @param supprew Supporting in reviews
   * @return List<Review>
  */
  public List<Review> supprew (String supprew) throws TimeoutException, ExecutionException, InterruptedException, ApiException {
     Object postBody = null;
  
      // verify the required parameter 'supprew' is set
      if (supprew == null) {
      VolleyError error = new VolleyError("Missing the required parameter 'supprew' when calling supprew",
      new ApiException(400, "Missing the required parameter 'supprew' when calling supprew"));
      }
  

  // create path and map variables
  String path = "/AdminSupport".replaceAll("\\{format\\}","json");

  // query params
  List<Pair> queryParams = new ArrayList<Pair>();
      // header params
      Map<String, String> headerParams = new HashMap<String, String>();
      // form params
      Map<String, String> formParams = new HashMap<String, String>();

          queryParams.addAll(ApiInvoker.parameterToPairs("", "supprew", supprew));


      String[] contentTypes = {
  
      };
      String contentType = contentTypes.length > 0 ? contentTypes[0] : "application/json";

      if (contentType.startsWith("multipart/form-data")) {
      // file uploading
      MultipartEntityBuilder localVarBuilder = MultipartEntityBuilder.create();
  

      HttpEntity httpEntity = localVarBuilder.build();
      postBody = httpEntity;
      } else {
      // normal form params
        }

      String[] authNames = new String[] {  };

      try {
        String localVarResponse = apiInvoker.invokeAPI (basePath, path, "GET", queryParams, postBody, headerParams, formParams, contentType, authNames);
        if(localVarResponse != null){
           return (List<Review>) ApiInvoker.deserialize(localVarResponse, "array", Review.class);
        } else {
           return null;
        }
      } catch (ApiException ex) {
         throw ex;
      } catch (InterruptedException ex) {
         throw ex;
      } catch (ExecutionException ex) {
         if(ex.getCause() instanceof VolleyError) {
	    VolleyError volleyError = (VolleyError)ex.getCause();
	    if (volleyError.networkResponse != null) {
	       throw new ApiException(volleyError.networkResponse.statusCode, volleyError.getMessage());
	    }
         }
         throw ex;
      } catch (TimeoutException ex) {
         throw ex;
      }
  }

      /**
   * Supporting in reviews
   * Supporting in reviews
   * @param supprew Supporting in reviews
  */
  public void supprew (String supprew, final Response.Listener<List<Review>> responseListener, final Response.ErrorListener errorListener) {
    Object postBody = null;

  
    // verify the required parameter 'supprew' is set
    if (supprew == null) {
       VolleyError error = new VolleyError("Missing the required parameter 'supprew' when calling supprew",
         new ApiException(400, "Missing the required parameter 'supprew' when calling supprew"));
    }
    

    // create path and map variables
    String path = "/AdminSupport".replaceAll("\\{format\\}","json");

    // query params
    List<Pair> queryParams = new ArrayList<Pair>();
    // header params
    Map<String, String> headerParams = new HashMap<String, String>();
    // form params
    Map<String, String> formParams = new HashMap<String, String>();

    queryParams.addAll(ApiInvoker.parameterToPairs("", "supprew", supprew));


    String[] contentTypes = {
      
    };
    String contentType = contentTypes.length > 0 ? contentTypes[0] : "application/json";

    if (contentType.startsWith("multipart/form-data")) {
      // file uploading
      MultipartEntityBuilder localVarBuilder = MultipartEntityBuilder.create();
      

      HttpEntity httpEntity = localVarBuilder.build();
      postBody = httpEntity;
    } else {
      // normal form params
          }

      String[] authNames = new String[] {  };

    try {
      apiInvoker.invokeAPI(basePath, path, "GET", queryParams, postBody, headerParams, formParams, contentType, authNames,
        new Response.Listener<String>() {
          @Override
          public void onResponse(String localVarResponse) {
            try {
              responseListener.onResponse((List<Review>) ApiInvoker.deserialize(localVarResponse,  "array", Review.class));
            } catch (ApiException exception) {
               errorListener.onErrorResponse(new VolleyError(exception));
            }
          }
      }, new Response.ErrorListener() {
          @Override
          public void onErrorResponse(VolleyError error) {
            errorListener.onErrorResponse(error);
          }
      });
    } catch (ApiException ex) {
      errorListener.onErrorResponse(new VolleyError(ex));
    }
  }
  /**
  * update salon
  * update salon
   * @param salonId id salona
   * @param regsalon salon
   * @return void
  */
  public void updatesalonPut (String salonId, Salon regsalon) throws TimeoutException, ExecutionException, InterruptedException, ApiException {
     Object postBody = regsalon;
  

  // create path and map variables
  String path = "/Updatesalon".replaceAll("\\{format\\}","json");

  // query params
  List<Pair> queryParams = new ArrayList<Pair>();
      // header params
      Map<String, String> headerParams = new HashMap<String, String>();
      // form params
      Map<String, String> formParams = new HashMap<String, String>();

          queryParams.addAll(ApiInvoker.parameterToPairs("", "salon id", salonId));


      String[] contentTypes = {
  
      };
      String contentType = contentTypes.length > 0 ? contentTypes[0] : "application/json";

      if (contentType.startsWith("multipart/form-data")) {
      // file uploading
      MultipartEntityBuilder localVarBuilder = MultipartEntityBuilder.create();
  

      HttpEntity httpEntity = localVarBuilder.build();
      postBody = httpEntity;
      } else {
      // normal form params
        }

      String[] authNames = new String[] {  };

      try {
        String localVarResponse = apiInvoker.invokeAPI (basePath, path, "PUT", queryParams, postBody, headerParams, formParams, contentType, authNames);
        if(localVarResponse != null){
           return ;
        } else {
           return ;
        }
      } catch (ApiException ex) {
         throw ex;
      } catch (InterruptedException ex) {
         throw ex;
      } catch (ExecutionException ex) {
         if(ex.getCause() instanceof VolleyError) {
	    VolleyError volleyError = (VolleyError)ex.getCause();
	    if (volleyError.networkResponse != null) {
	       throw new ApiException(volleyError.networkResponse.statusCode, volleyError.getMessage());
	    }
         }
         throw ex;
      } catch (TimeoutException ex) {
         throw ex;
      }
  }

      /**
   * update salon
   * update salon
   * @param salonId id salona   * @param regsalon salon
  */
  public void updatesalonPut (String salonId, Salon regsalon, final Response.Listener<String> responseListener, final Response.ErrorListener errorListener) {
    Object postBody = regsalon;

  

    // create path and map variables
    String path = "/Updatesalon".replaceAll("\\{format\\}","json");

    // query params
    List<Pair> queryParams = new ArrayList<Pair>();
    // header params
    Map<String, String> headerParams = new HashMap<String, String>();
    // form params
    Map<String, String> formParams = new HashMap<String, String>();

    queryParams.addAll(ApiInvoker.parameterToPairs("", "salon id", salonId));


    String[] contentTypes = {
      
    };
    String contentType = contentTypes.length > 0 ? contentTypes[0] : "application/json";

    if (contentType.startsWith("multipart/form-data")) {
      // file uploading
      MultipartEntityBuilder localVarBuilder = MultipartEntityBuilder.create();
      

      HttpEntity httpEntity = localVarBuilder.build();
      postBody = httpEntity;
    } else {
      // normal form params
          }

      String[] authNames = new String[] {  };

    try {
      apiInvoker.invokeAPI(basePath, path, "PUT", queryParams, postBody, headerParams, formParams, contentType, authNames,
        new Response.Listener<String>() {
          @Override
          public void onResponse(String localVarResponse) {
              responseListener.onResponse(localVarResponse);
          }
      }, new Response.ErrorListener() {
          @Override
          public void onErrorResponse(VolleyError error) {
            errorListener.onErrorResponse(error);
          }
      });
    } catch (ApiException ex) {
      errorListener.onErrorResponse(new VolleyError(ex));
    }
  }
}
