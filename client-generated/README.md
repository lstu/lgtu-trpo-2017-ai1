# swagger-android-client

## Requirements

Building the API client library requires [Maven](https://maven.apache.org/) to be installed.

## Installation

To install the API client library to your local Maven repository, simply execute:

```shell
mvn install
```

To deploy it to a remote Maven repository instead, configure the settings of the repository and execute:

```shell
mvn deploy
```

Refer to the [official documentation](https://maven.apache.org/plugins/maven-deploy-plugin/usage.html) for more information.

### Maven users

Add this dependency to your project's POM:

```xml
<dependency>
    <groupId>io.swagger</groupId>
    <artifactId>swagger-android-client</artifactId>
    <version>1.0.0</version>
    <scope>compile</scope>
</dependency>
```

### Gradle users

Add this dependency to your project's build file:

```groovy
compile "io.swagger:swagger-android-client:1.0.0"
```

### Others

At first generate the JAR by executing:

    mvn package

Then manually install the following JARs:

* target/swagger-android-client-1.0.0.jar
* target/lib/*.jar

## Getting Started

Please follow the [installation](#installation) instruction and execute the following Java code:

```java

import io.swagger.client.api.AdministrationApi;

public class AdministrationApiExample {

    public static void main(String[] args) {
        AdministrationApi apiInstance = new AdministrationApi();
        String salonId = "salonId_example"; // String | id salona
        try {
            apiInstance.deleteSalonDelete(salonId);
        } catch (ApiException e) {
            System.err.println("Exception when calling AdministrationApi#deleteSalonDelete");
            e.printStackTrace();
        }
    }
}

```

## Documentation for API Endpoints

All URIs are relative to *https://virtserver.swaggerhub.com/NoVaS/Salun/1.0.0*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*AdministrationApi* | [**deleteSalonDelete**](docs/AdministrationApi.md#deleteSalonDelete) | **DELETE** /DeleteSalon | delite salon
*AdministrationApi* | [**newsalon**](docs/AdministrationApi.md#newsalon) | **POST** /SalonReg | registr salon
*AdministrationApi* | [**supprew**](docs/AdministrationApi.md#supprew) | **GET** /AdminSupport | Supporting in reviews
*AdministrationApi* | [**updatesalonPut**](docs/AdministrationApi.md#updatesalonPut) | **PUT** /Updatesalon | update salon
*AdminsApi* | [**clientvisitstoryGet**](docs/AdminsApi.md#clientvisitstoryGet) | **GET** /clientvisitstory | story
*AdminsApi* | [**create**](docs/AdminsApi.md#create) | **POST** /ClientsCreatezai | createzai
*AdminsApi* | [**findclientGet**](docs/AdminsApi.md#findclientGet) | **GET** /Findclient | find
*AdminsApi* | [**markemp**](docs/AdminsApi.md#markemp) | **PUT** /AdminsMarkEmp | marking employers
*AdminsApi* | [**reg**](docs/AdminsApi.md#reg) | **POST** /Registration | registering client
*ClientsApi* | [**clientdelitevisitDelete**](docs/ClientsApi.md#clientdelitevisitDelete) | **DELETE** /Clientdelitevisit | delite visit
*ClientsApi* | [**create**](docs/ClientsApi.md#create) | **POST** /ClientsCreatezai | createzai
*ClientsApi* | [**log**](docs/ClientsApi.md#log) | **GET** /Login | login
*ClientsApi* | [**look**](docs/ClientsApi.md#look) | **GET** /ClientslookReview | look review
*ClientsApi* | [**makerew**](docs/ClientsApi.md#makerew) | **POST** /ClientsMakeReview | making rewiew
*ClientsApi* | [**reg**](docs/ClientsApi.md#reg) | **POST** /Registration | registering client
*DirectorsApi* | [**createserv**](docs/DirectorsApi.md#createserv) | **POST** /DirectorsCreateServ | Create services
*DirectorsApi* | [**directorsDeleteEmpDelete**](docs/DirectorsApi.md#directorsDeleteEmpDelete) | **DELETE** /DirectorsDeleteEmp | delite salon
*DirectorsApi* | [**directorsDeleteServDelete**](docs/DirectorsApi.md#directorsDeleteServDelete) | **DELETE** /DirectorsDeleteServ | deleteserv
*DirectorsApi* | [**directorsEditEmpPut**](docs/DirectorsApi.md#directorsEditEmpPut) | **PUT** /DirectorsEditEmp | update emp
*DirectorsApi* | [**directorsUpdateServPut**](docs/DirectorsApi.md#directorsUpdateServPut) | **PUT** /DirectorsUpdateServ | edit serv
*DirectorsApi* | [**newemp**](docs/DirectorsApi.md#newemp) | **POST** /DirectorsEmpReg | registr emp
*DirectorsApi* | [**posesh**](docs/DirectorsApi.md#posesh) | **GET** /Directorsposeh | look psesh
*EmployersApi* | [**log**](docs/EmployersApi.md#log) | **GET** /Login | login
*EmployersApi* | [**view**](docs/EmployersApi.md#view) | **GET** /EmployersView | view


## Documentation for Models

 - [AdminCredentials](docs/AdminCredentials.md)
 - [Clients](docs/Clients.md)
 - [Employers](docs/Employers.md)
 - [LevelAccess](docs/LevelAccess.md)
 - [Review](docs/Review.md)
 - [Salon](docs/Salon.md)
 - [Services](docs/Services.md)
 - [Shift](docs/Shift.md)
 - [Skills](docs/Skills.md)
 - [VisitRecord](docs/VisitRecord.md)


## Documentation for Authorization

All endpoints do not require authorization.
Authentication schemes defined for the API:

## Recommendation

It's recommended to create an instance of `ApiClient` per thread in a multithreaded environment to avoid any potential issues.

## Author

you@your-company.com

