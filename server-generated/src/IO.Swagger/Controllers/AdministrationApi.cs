/*
 * Simple Inventory API
 *
 * This is a simple API
 *
 * OpenAPI spec version: 1.0.0
 * Contact: you@your-company.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Swashbuckle.SwaggerGen.Annotations;
using IO.Swagger.Models;

namespace IO.Swagger.Controllers
{ 
    /// <summary>
    /// 
    /// </summary>
    public class AdministrationApiController : Controller
    { 

        /// <summary>
        /// delite salon
        /// </summary>
        /// <remarks>delete</remarks>
        /// <param name="salonId">id salona</param>
        /// <response code="200">OK</response>
        [HttpDelete]
        [Route("/NoVaS/Salun/1.0.0/DeleteSalon")]
        [SwaggerOperation("DeleteSalonDelete")]
        public virtual void DeleteSalonDelete([FromQuery]string salonId)
        { 
            throw new NotImplementedException();
        }


        /// <summary>
        /// registr salon
        /// </summary>
        /// <remarks>Registration new salon</remarks>
        /// <param name="telephone">tel</param>
        /// <param name="password">password</param>
        /// <param name="dirctorsName">directors name</param>
        /// <param name="directorsSecondName">second name</param>
        /// <param name="directorsLastName">last name</param>
        /// <param name="regsalon">salon</param>
        /// <response code="405">Invalid input</response>
        /// <response code="0">successful operation</response>
        [HttpPost]
        [Route("/NoVaS/Salun/1.0.0/SalonReg")]
        [SwaggerOperation("Newsalon")]
        public virtual void Newsalon([FromQuery]string telephone, [FromQuery]string password, [FromQuery]string dirctorsName, [FromQuery]string directorsSecondName, [FromQuery]string directorsLastName, [FromBody]Salon regsalon)
        { 
            throw new NotImplementedException();
        }


        /// <summary>
        /// Supporting in reviews
        /// </summary>
        /// <remarks>Supporting in reviews</remarks>
        /// <param name="supprew">Supporting in reviews</param>
        /// <response code="200">search results matching criteria</response>
        /// <response code="400">bad input parameter</response>
        [HttpGet]
        [Route("/NoVaS/Salun/1.0.0/AdminSupport")]
        [SwaggerOperation("Supprew")]
        [SwaggerResponse(200, type: typeof(List<Review>))]
        public virtual IActionResult Supprew([FromQuery]string supprew)
        { 
            string exampleJson = null;
            
            var example = exampleJson != null
            ? JsonConvert.DeserializeObject<List<Review>>(exampleJson)
            : default(List<Review>);
            return new ObjectResult(example);
        }


        /// <summary>
        /// update salon
        /// </summary>
        /// <remarks>update salon</remarks>
        /// <param name="salonId">id salona</param>
        /// <param name="regsalon">salon</param>
        /// <response code="200">OK</response>
        /// <response code="400">something wrong</response>
        [HttpPut]
        [Route("/NoVaS/Salun/1.0.0/Updatesalon")]
        [SwaggerOperation("UpdatesalonPut")]
        public virtual void UpdatesalonPut([FromQuery]string salonId, [FromBody]Salon regsalon)
        { 
            throw new NotImplementedException();
        }
    }
}
