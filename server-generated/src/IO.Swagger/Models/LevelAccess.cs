/*
 * Simple Inventory API
 *
 * This is a simple API
 *
 * OpenAPI spec version: 1.0.0
 * Contact: you@your-company.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Models
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public partial class LevelAccess :  IEquatable<LevelAccess>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LevelAccess" /> class.
        /// </summary>
        /// <param name="LA_ID">LA_ID (required).</param>
        /// <param name="LATitle">LATitle (required).</param>
        /// <param name="LACapabilites">LACapabilites (required).</param>
        public LevelAccess(string LA_ID = null, string LATitle = null, string LACapabilites = null)
        {
            // to ensure "LA_ID" is required (not null)
            if (LA_ID == null)
            {
                throw new InvalidDataException("LA_ID is a required property for LevelAccess and cannot be null");
            }
            else
            {
                this.LA_ID = LA_ID;
            }
            // to ensure "LATitle" is required (not null)
            if (LATitle == null)
            {
                throw new InvalidDataException("LATitle is a required property for LevelAccess and cannot be null");
            }
            else
            {
                this.LATitle = LATitle;
            }
            // to ensure "LACapabilites" is required (not null)
            if (LACapabilites == null)
            {
                throw new InvalidDataException("LACapabilites is a required property for LevelAccess and cannot be null");
            }
            else
            {
                this.LACapabilites = LACapabilites;
            }
            
        }

        /// <summary>
        /// Gets or Sets LA_ID
        /// </summary>
        [DataMember(Name="LA_ID")]
        public string LA_ID { get; set; }

        /// <summary>
        /// Gets or Sets LATitle
        /// </summary>
        [DataMember(Name="LA_Title")]
        public string LATitle { get; set; }

        /// <summary>
        /// Gets or Sets LACapabilites
        /// </summary>
        [DataMember(Name="LA_Capabilites")]
        public string LACapabilites { get; set; }


        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class LevelAccess {\n");
            sb.Append("  LA_ID: ").Append(LA_ID).Append("\n");
            sb.Append("  LATitle: ").Append(LATitle).Append("\n");
            sb.Append("  LACapabilites: ").Append(LACapabilites).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((LevelAccess)obj);
        }

        /// <summary>
        /// Returns true if LevelAccess instances are equal
        /// </summary>
        /// <param name="other">Instance of LevelAccess to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(LevelAccess other)
        {

            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return 
                (
                    this.LA_ID == other.LA_ID ||
                    this.LA_ID != null &&
                    this.LA_ID.Equals(other.LA_ID)
                ) && 
                (
                    this.LATitle == other.LATitle ||
                    this.LATitle != null &&
                    this.LATitle.Equals(other.LATitle)
                ) && 
                (
                    this.LACapabilites == other.LACapabilites ||
                    this.LACapabilites != null &&
                    this.LACapabilites.Equals(other.LACapabilites)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            // credit: http://stackoverflow.com/a/263416/677735
            unchecked // Overflow is fine, just wrap
            {
                int hash = 41;
                // Suitable nullity checks etc, of course :)
                    if (this.LA_ID != null)
                    hash = hash * 59 + this.LA_ID.GetHashCode();
                    if (this.LATitle != null)
                    hash = hash * 59 + this.LATitle.GetHashCode();
                    if (this.LACapabilites != null)
                    hash = hash * 59 + this.LACapabilites.GetHashCode();
                return hash;
            }
        }

        #region Operators

        public static bool operator ==(LevelAccess left, LevelAccess right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(LevelAccess left, LevelAccess right)
        {
            return !Equals(left, right);
        }

        #endregion Operators

    }
}
