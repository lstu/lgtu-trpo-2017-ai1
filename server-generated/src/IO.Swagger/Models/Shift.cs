/*
 * Simple Inventory API
 *
 * This is a simple API
 *
 * OpenAPI spec version: 1.0.0
 * Contact: you@your-company.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Models
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public partial class Shift :  IEquatable<Shift>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Shift" /> class.
        /// </summary>
        /// <param name="ShiftID">ShiftID (required).</param>
        /// <param name="EmpID">EmpID (required).</param>
        /// <param name="ShiftDate">ShiftDate (required).</param>
        /// <param name="ShiftBeginT">ShiftBeginT (required).</param>
        /// <param name="ShiftEndT">ShiftEndT (required).</param>
        /// <param name="ShiftStatus">ShiftStatus (required).</param>
        public Shift(string ShiftID = null, string EmpID = null, string ShiftDate = null, string ShiftBeginT = null, string ShiftEndT = null, string ShiftStatus = null)
        {
            // to ensure "ShiftID" is required (not null)
            if (ShiftID == null)
            {
                throw new InvalidDataException("ShiftID is a required property for Shift and cannot be null");
            }
            else
            {
                this.ShiftID = ShiftID;
            }
            // to ensure "EmpID" is required (not null)
            if (EmpID == null)
            {
                throw new InvalidDataException("EmpID is a required property for Shift and cannot be null");
            }
            else
            {
                this.EmpID = EmpID;
            }
            // to ensure "ShiftDate" is required (not null)
            if (ShiftDate == null)
            {
                throw new InvalidDataException("ShiftDate is a required property for Shift and cannot be null");
            }
            else
            {
                this.ShiftDate = ShiftDate;
            }
            // to ensure "ShiftBeginT" is required (not null)
            if (ShiftBeginT == null)
            {
                throw new InvalidDataException("ShiftBeginT is a required property for Shift and cannot be null");
            }
            else
            {
                this.ShiftBeginT = ShiftBeginT;
            }
            // to ensure "ShiftEndT" is required (not null)
            if (ShiftEndT == null)
            {
                throw new InvalidDataException("ShiftEndT is a required property for Shift and cannot be null");
            }
            else
            {
                this.ShiftEndT = ShiftEndT;
            }
            // to ensure "ShiftStatus" is required (not null)
            if (ShiftStatus == null)
            {
                throw new InvalidDataException("ShiftStatus is a required property for Shift and cannot be null");
            }
            else
            {
                this.ShiftStatus = ShiftStatus;
            }
            
        }

        /// <summary>
        /// Gets or Sets ShiftID
        /// </summary>
        [DataMember(Name="Shift_ID")]
        public string ShiftID { get; set; }

        /// <summary>
        /// Gets or Sets EmpID
        /// </summary>
        [DataMember(Name="Emp_ID")]
        public string EmpID { get; set; }

        /// <summary>
        /// Gets or Sets ShiftDate
        /// </summary>
        [DataMember(Name="Shift_Date")]
        public string ShiftDate { get; set; }

        /// <summary>
        /// Gets or Sets ShiftBeginT
        /// </summary>
        [DataMember(Name="Shift_BeginT")]
        public string ShiftBeginT { get; set; }

        /// <summary>
        /// Gets or Sets ShiftEndT
        /// </summary>
        [DataMember(Name="Shift_EndT")]
        public string ShiftEndT { get; set; }

        /// <summary>
        /// Gets or Sets ShiftStatus
        /// </summary>
        [DataMember(Name="Shift_Status")]
        public string ShiftStatus { get; set; }


        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Shift {\n");
            sb.Append("  ShiftID: ").Append(ShiftID).Append("\n");
            sb.Append("  EmpID: ").Append(EmpID).Append("\n");
            sb.Append("  ShiftDate: ").Append(ShiftDate).Append("\n");
            sb.Append("  ShiftBeginT: ").Append(ShiftBeginT).Append("\n");
            sb.Append("  ShiftEndT: ").Append(ShiftEndT).Append("\n");
            sb.Append("  ShiftStatus: ").Append(ShiftStatus).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Shift)obj);
        }

        /// <summary>
        /// Returns true if Shift instances are equal
        /// </summary>
        /// <param name="other">Instance of Shift to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Shift other)
        {

            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return 
                (
                    this.ShiftID == other.ShiftID ||
                    this.ShiftID != null &&
                    this.ShiftID.Equals(other.ShiftID)
                ) && 
                (
                    this.EmpID == other.EmpID ||
                    this.EmpID != null &&
                    this.EmpID.Equals(other.EmpID)
                ) && 
                (
                    this.ShiftDate == other.ShiftDate ||
                    this.ShiftDate != null &&
                    this.ShiftDate.Equals(other.ShiftDate)
                ) && 
                (
                    this.ShiftBeginT == other.ShiftBeginT ||
                    this.ShiftBeginT != null &&
                    this.ShiftBeginT.Equals(other.ShiftBeginT)
                ) && 
                (
                    this.ShiftEndT == other.ShiftEndT ||
                    this.ShiftEndT != null &&
                    this.ShiftEndT.Equals(other.ShiftEndT)
                ) && 
                (
                    this.ShiftStatus == other.ShiftStatus ||
                    this.ShiftStatus != null &&
                    this.ShiftStatus.Equals(other.ShiftStatus)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            // credit: http://stackoverflow.com/a/263416/677735
            unchecked // Overflow is fine, just wrap
            {
                int hash = 41;
                // Suitable nullity checks etc, of course :)
                    if (this.ShiftID != null)
                    hash = hash * 59 + this.ShiftID.GetHashCode();
                    if (this.EmpID != null)
                    hash = hash * 59 + this.EmpID.GetHashCode();
                    if (this.ShiftDate != null)
                    hash = hash * 59 + this.ShiftDate.GetHashCode();
                    if (this.ShiftBeginT != null)
                    hash = hash * 59 + this.ShiftBeginT.GetHashCode();
                    if (this.ShiftEndT != null)
                    hash = hash * 59 + this.ShiftEndT.GetHashCode();
                    if (this.ShiftStatus != null)
                    hash = hash * 59 + this.ShiftStatus.GetHashCode();
                return hash;
            }
        }

        #region Operators

        public static bool operator ==(Shift left, Shift right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Shift left, Shift right)
        {
            return !Equals(left, right);
        }

        #endregion Operators

    }
}
