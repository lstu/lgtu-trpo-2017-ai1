/*
 * Simple Inventory API
 *
 * This is a simple API
 *
 * OpenAPI spec version: 1.0.0
 * Contact: you@your-company.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Models
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public partial class Review :  IEquatable<Review>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Review" /> class.
        /// </summary>
        /// <param name="RevDT">RevDT (required).</param>
        /// <param name="SalonID">SalonID (required).</param>
        /// <param name="ClientID">ClientID (required).</param>
        /// <param name="RevTheme">RevTheme (required).</param>
        /// <param name="RevText">RevText (required).</param>
        public Review(string RevDT = null, string SalonID = null, string ClientID = null, string RevTheme = null, string RevText = null)
        {
            // to ensure "RevDT" is required (not null)
            if (RevDT == null)
            {
                throw new InvalidDataException("RevDT is a required property for Review and cannot be null");
            }
            else
            {
                this.RevDT = RevDT;
            }
            // to ensure "SalonID" is required (not null)
            if (SalonID == null)
            {
                throw new InvalidDataException("SalonID is a required property for Review and cannot be null");
            }
            else
            {
                this.SalonID = SalonID;
            }
            // to ensure "ClientID" is required (not null)
            if (ClientID == null)
            {
                throw new InvalidDataException("ClientID is a required property for Review and cannot be null");
            }
            else
            {
                this.ClientID = ClientID;
            }
            // to ensure "RevTheme" is required (not null)
            if (RevTheme == null)
            {
                throw new InvalidDataException("RevTheme is a required property for Review and cannot be null");
            }
            else
            {
                this.RevTheme = RevTheme;
            }
            // to ensure "RevText" is required (not null)
            if (RevText == null)
            {
                throw new InvalidDataException("RevText is a required property for Review and cannot be null");
            }
            else
            {
                this.RevText = RevText;
            }
            
        }

        /// <summary>
        /// Gets or Sets RevDT
        /// </summary>
        [DataMember(Name="Rev_DT")]
        public string RevDT { get; set; }

        /// <summary>
        /// Gets or Sets SalonID
        /// </summary>
        [DataMember(Name="Salon_ID")]
        public string SalonID { get; set; }

        /// <summary>
        /// Gets or Sets ClientID
        /// </summary>
        [DataMember(Name="Client_ID")]
        public string ClientID { get; set; }

        /// <summary>
        /// Gets or Sets RevTheme
        /// </summary>
        [DataMember(Name="Rev_Theme")]
        public string RevTheme { get; set; }

        /// <summary>
        /// Gets or Sets RevText
        /// </summary>
        [DataMember(Name="Rev_Text")]
        public string RevText { get; set; }


        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Review {\n");
            sb.Append("  RevDT: ").Append(RevDT).Append("\n");
            sb.Append("  SalonID: ").Append(SalonID).Append("\n");
            sb.Append("  ClientID: ").Append(ClientID).Append("\n");
            sb.Append("  RevTheme: ").Append(RevTheme).Append("\n");
            sb.Append("  RevText: ").Append(RevText).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Review)obj);
        }

        /// <summary>
        /// Returns true if Review instances are equal
        /// </summary>
        /// <param name="other">Instance of Review to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Review other)
        {

            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return 
                (
                    this.RevDT == other.RevDT ||
                    this.RevDT != null &&
                    this.RevDT.Equals(other.RevDT)
                ) && 
                (
                    this.SalonID == other.SalonID ||
                    this.SalonID != null &&
                    this.SalonID.Equals(other.SalonID)
                ) && 
                (
                    this.ClientID == other.ClientID ||
                    this.ClientID != null &&
                    this.ClientID.Equals(other.ClientID)
                ) && 
                (
                    this.RevTheme == other.RevTheme ||
                    this.RevTheme != null &&
                    this.RevTheme.Equals(other.RevTheme)
                ) && 
                (
                    this.RevText == other.RevText ||
                    this.RevText != null &&
                    this.RevText.Equals(other.RevText)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            // credit: http://stackoverflow.com/a/263416/677735
            unchecked // Overflow is fine, just wrap
            {
                int hash = 41;
                // Suitable nullity checks etc, of course :)
                    if (this.RevDT != null)
                    hash = hash * 59 + this.RevDT.GetHashCode();
                    if (this.SalonID != null)
                    hash = hash * 59 + this.SalonID.GetHashCode();
                    if (this.ClientID != null)
                    hash = hash * 59 + this.ClientID.GetHashCode();
                    if (this.RevTheme != null)
                    hash = hash * 59 + this.RevTheme.GetHashCode();
                    if (this.RevText != null)
                    hash = hash * 59 + this.RevText.GetHashCode();
                return hash;
            }
        }

        #region Operators

        public static bool operator ==(Review left, Review right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Review left, Review right)
        {
            return !Equals(left, right);
        }

        #endregion Operators

    }
}
